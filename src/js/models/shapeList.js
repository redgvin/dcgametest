function ShapeList() {
  this.shapes = Object.create(null);
  this.shapeCntIndex = 0;
}

ShapeList.prototype = {
  // without PIXI.Application() can not calc Shape Area
  createShape: function(e, app, gameSize) {
    var dataGlobal = (e && e.data.global) || null;
    var shape = new Shape(this.shapeCntIndex);
    var shapeArea = this.calculateShapeArea(shape.shape, app);

    shape.setBounds(gameSize);
    shape.position(dataGlobal);
    shape.setArea(shapeArea);

    this.shapes[this.shapeCntIndex] = shape;
    this.shapeCntIndex++;
    app.stage.addChild(shape.shape);

    return shape;
  },

  removeShape: function (index, isPointerTap) {
    var area = 0;
    if (this.shapes[index].destroy(isPointerTap)) {
      // with minus because destroy
      area = -this.shapes[index].area;
      delete this.shapes[index];
    }

    return area
  },

  // without PIXI.Application() can not calc Shape Area
  calculateShapeArea: function (shape, app) {
    // calculate shape area
    var data = app.renderer.plugins.extract.pixels(shape);
    var cnt = 0;
    for (var i=0, len = data.length;i<len;i+=4) {
      if(data[i+3] !==0){ cnt++;}
    }

    return cnt;
  }
};