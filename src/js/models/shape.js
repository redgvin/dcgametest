// shape class
function Shape(index, name) {
  this.shape = new PIXI.Graphics();
  this.index = index;
  this.name = name || this.getRandomName();
  this.shape.interactive = true;
  this.gameSize = {
    width: 800,
    height: 600
  };

  this.shape.beginFill(gHelpers.getRandomColor());
  this[this.name]();
  this.shape.endFill();

  // Maybe we want to add transform
  // this.shape.setTransform(200, 200, 1, 1, 2, 0, 0)
}

Shape.prototype = {
  getRandomName: function () {
    var FIGURE_NAME = [
      'triangle',
      'rectangle',
      'pentagon',
      'hexagon',
      'circle',
      'ellipse',
      'random'
    ];
    var index = gHelpers.getRandomInt(0, FIGURE_NAME.length - 1);
    return FIGURE_NAME[index];
  },

  setBounds: function (size) {
    this.gameSize = size;
  },

  setArea: function (area) {
    this.area = area;
  },

  triangle: function () {
    this.shape.drawPolygon([
      -32, -64,
      32, -64,
      0, 0
    ]);
  },

  rectangle: function () {
    this.shape.drawRect(0, 0, -64, -64);
    this.shape.hitArea = this.shape.getBounds();
  },

  pentagon: function () {
    this.shape.drawPolygon([
      0, 0,
      0, -32,
      -32, -48,
      -64, -32,
      -64, 0
    ]);
  },

  hexagon: function(){
    this.shape.drawPolygon([
      0, -16,
      0, -48,
      -32, -64,
      -64, -48,
      -64, -16,
      -32, 0
    ]);
  },

  circle: function () {
    this.shape.drawCircle(0, -32, 32);
  },

  ellipse: function () {
    this.shape.drawEllipse(0, -20, 50, 30);
  },

  random: function () {
    this.shape.moveTo(0,0);
    this.shape.bezierCurveTo(0,0, 0,-20, 20,-10);
    this.shape.bezierCurveTo(20,-10, 50,-15,50,10);
    this.shape.bezierCurveTo(50,10, 100,40, 50,70);
    this.shape.bezierCurveTo(50,70, 40,100, 20,70);
    this.shape.bezierCurveTo(20,70, -10,70, 0,30);
    this.shape.bezierCurveTo(0,30, -30,10, 0,0);
  },

  position: function (moveTo) {
    var bounds = this.shape.getBounds();
    var defaultY = -(bounds.y + bounds.height);
    var deltaXLeft = (bounds.x <= 0) ?-bounds.x : 0;
    var deltaXRight = bounds.x + bounds.width;
    deltaXRight = this.gameSize.width - deltaXRight;

    // on click generate every new shape inside game rectangle
    if (moveTo) {
      this.shape.x = moveTo.x;
      this.shape.y = moveTo.y;
      if (this.name === 'rectangle') {
        if (moveTo.x - this.shape.width < 0) {
          this.shape.x = this.shape.width;
        }
        if (moveTo.y - this.shape.height < 0) {
          this.shape.y = this.shape.height
        }
      } else {
        var newBounds = this.shape.getBounds();
        if (newBounds.x < 0) {
          this.shape.x = deltaXLeft;
        }
        if (newBounds.x + newBounds.width > this.gameSize.width) {
          this.shape.x = deltaXRight;
        }
        if (newBounds.y < 0) {
          this.shape.y = newBounds.height;
        }
        if (newBounds.y + newBounds.height > this.gameSize.height) {
          this.shape.y = this.gameSize.height - newBounds.height;
        }
      }
    } else {
      this.shape.x = gHelpers.getRandomInt(deltaXLeft, deltaXRight);
      this.shape.y = defaultY;
    }
  },

  destroy: function (isPointertap) {
    var isUnderBottom = false;
    if (this.name === 'rectangle') {
      isUnderBottom = this.shape.y > this.gameSize.height;
    } else {
      var bounds = this.shape.getBounds();
      isUnderBottom = bounds.y + bounds.height > this.gameSize.height;
    }

    if (isPointertap || isUnderBottom) {
      this.shape.destroy();
      return true;
    }

  }

};