// html controls class
function Controls(model) {
  this.model = model;
  this.shapeCount = document.getElementById("shape_count");
  this.gravityBlock = document.getElementById("gravity");
  this.shapeCountInput = this.shapeCount.querySelector('input');
  this.gravityBlockInput = this.gravityBlock.querySelector('input');
  this.numberOfSapes = document.getElementById('number');
  this.totalArea = document.getElementById('area');
}

Controls.prototype = {
  init: function (opt) {
    var defaultOpt = {
      inputConstrains: {
        max: 100,
        min: 0
      },
      GAME_SIZE: {width: 800,height: 600}
    };
    this.opt = Object.assign(defaultOpt, opt);

    this.gravity = 1;
    this.animationCount = 0;
    this.perSecond = 1;
    this.numberOfCurrentShapes = 0;
    this.area = 0;
    this.createApp();
    this.bindsEvents();
  },

  createApp: function () {
    // Create the application
    this.app = new PIXI.Application();
    this.app.renderer.view.style.border = "1px dashed black";
    this.app.renderer.view.style.display = "block";
    this.app.renderer.autoResize = true;
    this.app.renderer.resize(this.opt.GAME_SIZE.width, this.opt.GAME_SIZE.height);

    // main rectangle
    var graphics = new PIXI.Graphics();
    graphics.beginFill(0x061639);
    graphics.drawRect(0, 0, this.opt.GAME_SIZE.width, this.opt.GAME_SIZE.height);
    graphics.endFill();
    graphics.x = 0;
    graphics.y = 0;
    this.app.stage.addChild(graphics);

    // Add the view to the DOM
    document.body.appendChild(this.app.view);
    this.app.stage.interactive = true;
    this.app.stop();

    this.addTiker();
    this.app.start();
  },

  addTiker: function () {
    var self = this;
    this.app.ticker.add(function() {
      self.generateOnStep = Math.round(60 / self.perSecond);
      if (self.animationCount % self.generateOnStep === 0 && self.gravity > 0) {
        self.createShape();
      }

      self.updateShapeList();

      self.animationCount++;
    });
  },
  
  createShape: function (e) {
    var shape = this.model.createShape(e, this.app, this.opt.GAME_SIZE);
    this.pointerTap(shape);
    this.updateTotalArea(shape.area);
    this.updateShapeCount();
  },

  removeShape: function (idx, isPointerTap) {
    var area = this.model.removeShape(idx, isPointerTap);
    this.updateTotalArea(area);
    this.updateShapeCount();
  },

  updateShapeCount: function () {
    var len = Object.keys(this.model.shapes).length;
    if (this.numberOfCurrentShapes !== len) {
      this.numberOfCurrentShapes = len;
      this.numberOfSapes.innerHTML = this.numberOfCurrentShapes;
    }
  },

  updateTotalArea: function (area) {
    this.area += area;
    this.totalArea.innerHTML = this.area;
  },

  updateShapeList: function () {
    for(var idx in this.model.shapes) {
      this.model.shapes[idx].shape.y += this.gravity;
      this.removeShape(idx)
    }
  },

  pointerTap: function (shape) {
    var self = this;
    shape.shape.on('pointertap', function (e) {
      e.stopPropagation();
      self.removeShape(shape.index, true)
    })
  },

  bindsEvents: function () {
    var self = this;
    this.shapeCount.addEventListener("click", function(e) {
      self.perSecond = self.processClicks.call(this, e, self.validateInput.bind(self))
    });

    this.gravityBlock.addEventListener("click", function(e) {
      self.gravity = self.processClicks.call(this, e, self.validateInput.bind(self))
    });

    this.shapeCountInput.addEventListener("input", function() {
      var value = self.validateInput(this.value);
      if (value !=='') self.perSecond = value;
      this.value = value;
    });

    this.gravityBlockInput.addEventListener("input", function() {
      var value = self.validateInput(this.value);
      if (value !=='') self.gravity = value;
      this.value = value;
    });

    this.app.stage.on('pointertap', this.createShape.bind(this));
  },

  processClicks: function (e, validateInput) {
    var targetClass = e.target.getAttribute('class');
    var input = this.querySelector('input');
    var value = validateInput(input.value);

    if (targetClass === 'minus') {
      value --;
    } else if (targetClass === 'plus') {
      value ++;
    }

    value = validateInput(value);
    input.value = value;

    return value;
  },

  validateInput: function (value) {
    if (value.length === 0) {
      return ''
    }

    value = parseFloat(value);
    if (isNaN(value)) {
      value = 1
    }

    if (value < this.opt.inputConstrains.min) {
      value = this.opt.inputConstrains.min
    }
    if (value > this.opt.inputConstrains.max) {
      value = this.opt.inputConstrains.max
    }

    return value;
  }

};