
# How to run

0. Clone git repository

```
git clone git@bitbucket.org:redgvin/dcgametest.git
```
repository page https://bitbucket.org/redgvin/dcgametest

1. Go to the cloned repository and open the index.html file in any browser with WebGL or Canvas support

```
cd dcgametest/src
```

##### shape.js, shapeList.js as model
##### controls.js as controller
##### index.html as view

### Game screen
![alt text](screenshots/1.png "Game screen")

